# Crazybin Samba

Crazybin Samba is a versatile container image that wraps the powerful Samba server, providing seamless file sharing capabilities in your containerized environment. Built on the lightweight and secure Alpine Linux base, this image offers an efficient solution for setting up and managing a Samba server with ease.

## What is Samba

<https://www.samba.org/>

Samba is the standard Windows interoperability suite of programs for Linux and Unix.

Since 1992, Samba has provided secure, stable and fast file and print services for all clients using the SMB/CIFS protocol, such as all versions of DOS and Windows, OS/2, Linux and many others.

Samba is an important component to seamlessly integrate Linux/Unix Servers and Desktops into Active Directory environments. It can function both as a domain controller or as a regular domain member.

Samba is a software package that gives network administrators flexibility and freedom in terms of setup, configuration, and choice of systems and equipment. Because of all that it offers, Samba has grown in popularity, and continues to do so, every year since its release in 1992.

## Usage

Setting up Crazybin Samba is straightforward. To add users, simply mount a volume at /container/users, where each user's name corresponds to a file name, and the file content contains the user's password. Similarly, to add shares, mount a volume at /container/sections, with each section's name represented as a file name and the file content containing the section's configuration. This simple approach enables easy user and share management within the container.

The default smb.conf file provided by Crazybin Samba includes the following values:
``` toml
[global]
log level = 3
min protocol = SMB3
disable netbios = yes
server role = standalone server
security = user
force user = root
```

You can override these values by adding a custom global section to /container/sections to achieve the desired configuration.

### Podman run

``` sh
podman run -d \
  -p 445:445 \
  -v /path/to/users:/container/users \
  -v /path/to/sections:/container/sections \
  quay.io/crazybin/samba:latest
```

Replace /path/to/users and /path/to/sections with the respective paths on your host machine where you store the user and section files. This command maps the necessary volumes and exposes the Samba server on port 445 for easy access.

### Podman kube

Create config in kube.yml:

``` yaml
apiVersion: v1
kind: Secret
metadata:
  name: users
data:
  foo: Zm9v # base64("foo")
  bar: YmFy # base64("bar")
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: sections
data:
  global: |
    log level = 5
  foo: |
    path = /container/data/foo
    valid users = foo
  public: |
    path = /container/data/public
    public = yes
---
apiVersion: v1
kind: Pod
metadata:
  labels:
    app: main
  name: main
spec:
  containers:
  - image: quay.io/crazybin/samba
    name: samba
    ports:
    - containerPort: 445
      hostPort: 445
    volumeMounts:
    - name: users
      mountPath: /container/users
    - name: sections
      mountPath: /container/sections
    - name: data
      mountPath: /container/data
  volumes:
  - name: users
    secret:
      secretName: users
  - name: sections
    configMap:
      name: sections
  - name: data
    hostPath:
      path: PATH_TO_DATA
      type: Directory
```

Play config:

``` sh
podman kube play kube.yml
```

## Join the Crazybin community

Crazybin Samba is an open-source project hosted on GitLab. You can find the source code, contribute, and explore additional information, documentation, and updates by visiting our GitLab repository:

<https://gitlab.com/crazybin/samba>

Unlock the full potential of your file sharing capabilities with Crazybin Samba - the efficient Samba server container image.
