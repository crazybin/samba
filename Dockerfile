FROM docker.io/library/alpine:latest as uid_wrapper
RUN apk add alpine-sdk cmake
RUN git clone https://git.samba.org/uid_wrapper.git
RUN mkdir uid_wrapper/build
WORKDIR uid_wrapper/build
RUN cmake -DCMAKE_INSTALL_PREFIX=../out -DCMAKE_BUILD_TYPE=Release ..
RUN make install

FROM docker.io/library/alpine:latest
RUN apk upgrade --no-cache
RUN apk add --no-cache samba s6-overlay
COPY --from=uid_wrapper /uid_wrapper/out /
COPY /root /
EXPOSE 445
ENTRYPOINT ["/init"]
